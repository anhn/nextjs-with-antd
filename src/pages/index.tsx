import { Layout, Typography, Menu } from 'antd'

const { Header, Content } = Layout
const { Paragraph } = Typography

const Page = () => {
    const content = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi'
    return <Layout>
        <Header>
            <Menu theme="dark" mode="horizontal">
                <Menu.Item key="1">List</Menu.Item>
                <Menu.Item key="2">More</Menu.Item>
            </Menu>
        </Header>
        <Content style={{ padding: 24 }}>
            <Paragraph ellipsis={{ rows: 1, expandable: true, symbol: 'more' }}>{content}</Paragraph>
        </Content>
    </Layout>
}

export default Page