import { AppProps } from 'next/app'

//Global antd styles
import 'antd/dist/antd.less'

function MyApp({ Component, pageProps /*, router*/ }: AppProps) {
  return <Component {...pageProps} />
}

export default MyApp
